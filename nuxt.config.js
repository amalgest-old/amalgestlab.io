import shrinkRay from 'shrink-ray-current'
const pkg = require('./package') // Brotli Compressor

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    htmlAttrs: {
      lang: 'fr'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'author', content: 'Bastien Pederencino' },
      { hid: 'description', name: 'description', content: pkg.description },
      {
        name: 'keywords',
        content:
          'Légumes, Plaisir, Famille, Enfant, Culture, Créer du lien, Bien manger, Partage, Expérience'
      },

      { property: 'og:url', content: 'https://cultureasy.netlify.com' },
      { property: 'og:type', content: 'website' },
      { property: 'og:title', content: 'Cultureasy' },
      {
        property: 'og:image',
        content: 'https://bpederencino.gitlab.io/bpederencino.png'
      },
      {
        property: 'og:description',
        content:
          "Cultiver les légumes pour que les manger en famille devienne un plaisir. Pour que l'art des légumes deviennent un jeu d'enfant."
      },
      { property: 'og:site_name', content: 'Cultureasy' },
      { property: 'og:locale', content: 'fr_FR' },
      { property: 'article:author', content: 'Bastien Pederencino' },

      { property: 'twitter:card', content: 'summary' },
      { property: 'twitter:site', content: '@BPederencino' },
      { property: 'twitter:creator', content: '@BPederencino' },
      { property: 'twitter:url', content: 'https://cultureasy.netlify.com' },
      { property: 'twitter:title', content: 'Cultureasy' },
      {
        property: 'twitter:description',
        content:
          "Cultiver les légumes pour que les manger en famille devienne un plaisir.Pour que l'art des légumes deviennent un jeu d'enfant."
      },
      {
        property: 'twitter:image',
        content: 'https://bpederencino.gitlab.io/bpederencino.png'
      },

      { name: 'application-name', content: 'BPederencino' },
      { name: 'msapplication-square70x70logo', content: '' },
      { name: 'msapplication-square150x150logo', content: '' },
      { name: 'msapplication-wide310x150logo', content: '' },
      { name: 'msapplication-square310x310logo', content: '' },
      { name: 'msapplication-TileColor', content: '#202126' },

      { name: 'robots', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', href: '/favicon.ico' },
      { rel: 'icon', type: 'image/png', href: '/favicon.png' },
      { rel: 'author', href: 'humans.txt' }
    ],
    title: 'Cultureasy' // pkg.name
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
   ** Customize the generated output folder
   */
  generate: {
    dir: 'public'
  },

  /*
   ** Customize the base url
   */
  router: {
    base: '/'
  },

  /*
  ** Global CSS
  */
  css: [],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://buefy.github.io/#/documentation
    ['nuxt-buefy', { css: true, materialDesignIcons: false }],
    '@nuxtjs/pwa'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
   ** Render optimisation
   */
  render: {
    compressor: shrinkRay() // See https://blog.lichter.io/posts/nuxtjs-on-brotli/
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
