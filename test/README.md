# STATIC

**This directory is not required, you can delete it if you don't want to use it.**

This directory contains your test files.

More information about the usage of this directory in [Jest documentation](https://jestjs.io/docs/en/getting-started.html).
