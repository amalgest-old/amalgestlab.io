# WebApp

> WebApp of Cultureasy©2019

## Goals

Develop a delightful web application template with Nuxt.js.

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate

# do unit tests
$ npm run test
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## Usage

### HEAD Settings

Update head and meta data in _nuxt.config.js_ in `head: {}` (line 10).

First, the title of your web app: `title: 'JEECE'` (line 70).
Then, change the "lang" value (line 12):

```
htmlAttrs: {
  lang: 'fr'
}
```

And finally, update `meta: []` (line 14) with meta data and `link: []` (line 64) with all external files and stylesheets.

### STYLE Settings

Update fonts in 'assets/fonts' folder with a TTF file like "FontName.ttf".
Then, import this font in 'layouts/default.scss' (line 68):

```
@font-face {
  /* Describe the scope of this font */
  font-family: 'FontName';
  src: url(~assets/fonts/FontName.ttf);
}
```

And add the font name in family primary or secondary, for instance: `$family-secondary: 'FontName';` (line 90).

### Testing

This Nuxt template use AVA and JSDom to test component.
Create test in _tests_ folder, and name it _componentName.test.js_ (AVA test only _.test.js_ files). Look at _tests/example.test.js_ to see how it works.
Then use the command `npm test:e2e` to launch E2E test.

## Status

## Owners / Maintainers

- Bastien PEDERENCINO [bastien.pederencino@amalgest.com](mailto:bastien.pederencino@amalgest.com)

## Contribute

## Code of Conduct

## License
